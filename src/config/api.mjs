export const WEBSERVER_PORT = 80;

// Set these to whatever your actual IPs are
// Should be visible at: https://console.cloud.google.com/kubernetes/discovery
export const FRUIT_URL = "http://fruit:2000";
export const GREETER_URL = "http://greeter:3000";
